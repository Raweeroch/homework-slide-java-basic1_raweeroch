package HM;

public class Homework3 {
	public static void main(String[] args) {
		System.out.println("draw9");
	    draw9(2);
	    System.out.println("\ndraw10");
	    draw10(3);
	    System.out.println("\ndraw11");
	    draw11(4);
	    System.out.println("\ndraw12");
	    draw12(2);
	    System.out.println("\ndraw13");
	    draw13(2);
	    System.out.println("\ndraw14");
	    draw14(3);
	    System.out.println("\ndraw15");
	    draw15(4);
	    System.out.println("\ndraw16");
	    draw16(2);
	    System.out.println("\ndraw17");
	    draw17(3);
		
	}
	 public static void draw9(int n) {
		    int num = -2;
		    for (int i = 0; i < n; i++) {
		      num += 2;
		      System.out.println(num);
		    } 
		  }
		  
		  public static void draw10(int n) {
		    int num = 0;
		    for (int i = 0; i < n; i++) {
		      num += 2;
		      System.out.println(num);
		    } 
		  }
		  
		  public static void draw11(int n) {
		    int idex = 1;
		    int idexRow = 2;
		    for (int i = 0; i < n; i++) {
		      for (int t = 0; t < n; t++) {
		        System.out.print(String.valueOf(idex) + " ");
		        idex = idex + i + 1;
		      } 
		      idex = idexRow++;
		      System.out.println();
		    } 
		  }
		  
		  public static void draw12(int n) {
		    String[] s = new String[n];
		    int i;
		    for (i = 0; i < n; i++)
		      s[i] = "*"; 
		    for (i = 0; i < n; i++) {
		      s[i] = "_";
		      for (int t = 0; t < n; t++)
		        System.out.print(s[t]); 
		      s[i] = "*";
		      System.out.println();
		    } 
		  }
		  
		  public static void draw13(int n) {
		    String[] s = new String[n];
		    int i;
		    for (i = 0; i < n; i++)
		      s[i] = "*"; 
		    for (i = n; i > 0; i--) {
		      s[i - 1] = "_";
		      for (int t = 0; t < n; t++)
		        System.out.print(s[t]); 
		      s[i - 1] = "*";
		      System.out.println();
		    } 
		  }
		  
		  public static void draw14(int n) {
		    String[] s = new String[n];
		    int i;
		    for (i = 0; i < n; i++)
		      s[i] = "_"; 
		    for (i = 0; i < n; i++) {
		      s[i] = "*";
		      for (int t = 0; t < n; t++)
		        System.out.print(String.valueOf(s[t]) + " "); 
		      System.out.println();
		    } 
		  }
		  
		  public static void draw15(int n) {
		    String[] s = new String[n];
		    int i;
		    for (i = 0; i < n; i++)
		      s[i] = "*"; 
		    for (i = n; i > 0; i--) {
		      for (int t = 0; t < n; t++)
		        System.out.print(String.valueOf(s[t]) + " "); 
		      s[i - 1] = "_";
		      System.out.println();
		    } 
		  }
		  
		  public static void draw16(int n) {
		    String[] s = new String[n];
		    int i;
		    for (i = 0; i < n; i++)
		      s[i] = "_"; 
		    for (i = 0; i < n; i++) {
		      s[i] = "*";
		      for (int t = 0; t < n; t++)
		        System.out.print(String.valueOf(s[t]) + " "); 
		      System.out.println();
		    } 
		    for (i = n - 1; i > 0; i--) {
		      s[i] = "_";
		      for (int t = 0; t < n; t++)
		        System.out.print(String.valueOf(s[t]) + " "); 
		      System.out.println();
		    } 
		  }
		  
		  public static void draw17(int n) {
		    String[] s = new String[n];
		    for (int y = 0; y < n; y++)
		      s[y] = "-"; 
		    int i;
		    for (i = 0; i < n; i++) {
		      for (int g = 0; g <= i; g++)
		        s[g] = String.valueOf(i + 1); 
		      for (int t = 0; t < n; t++)
		        System.out.print(s[t]); 
		      System.out.println();
		    } 
		    for (i = n - 1; i > 0; i--) {
		      for (int e = 0; e <= i; e++)
		        s[e] = String.valueOf(i); 
		      for (int g = i; g >= i; g--)
		        s[g] = "-"; 
		      for (int t = 0; t < n; t++)
		        System.out.print(s[t]); 
		      System.out.println();
		    } 
		  }
		
}
